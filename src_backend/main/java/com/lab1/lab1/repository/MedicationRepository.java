package com.lab1.lab1.repository;

import com.lab1.lab1.entities.Caregiver;
import com.lab1.lab1.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationRepository extends JpaRepository<Medication,Long> {
    Medication findById(long id);
    void deleteById(long id);
    Medication findByName(String name);
}
