package com.lab1.lab1.repository;

import com.lab1.lab1.entities.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorRepository extends JpaRepository<Doctor,Long> {
    Doctor findById(long id);
    void deleteById(long id);
}
