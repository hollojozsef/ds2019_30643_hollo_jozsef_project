package com.lab1.lab1.endpoint;

import com.lab1.lab1.entities.Medication;
import com.lab1.lab1.service.MedicationService;
import com.techprimers.spring_boot_soap_example.GetMedicationRequest;
import com.techprimers.spring_boot_soap_example.GetMedicationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class MedicationEndpoint {
    @Autowired
    MedicationService medicationService;

    @PayloadRoot(namespace = "http://techprimers.com/spring-boot-soap-example",
    localPart="getMedicationRequest")
    @ResponsePayload
    public GetMedicationResponse getMedicationRequest(@RequestPayload GetMedicationRequest request){
        GetMedicationResponse response=new GetMedicationResponse();
        Medication medication=medicationService.findByName(request.getName());
        com.techprimers.spring_boot_soap_example.Medication soapMedication=new com.techprimers.spring_boot_soap_example.Medication();
        soapMedication.setName(medication.getName());
        soapMedication.setDosage(medication.getDosage());
        soapMedication.setSideEffect(medication.getSideEffects());
        response.setMedication(soapMedication);
        return response;
    }

}
