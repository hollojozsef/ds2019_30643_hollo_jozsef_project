package com.lab1.lab1;

import com.lab1.lab1.entities.Patient;
import com.lab1.lab1.service.MedicationServiceGrpc;
import com.lab1.lab1.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.io.IOException;

@SpringBootApplication
public class Lab1Application {
	@Autowired
	MedicationServiceGrpc medicationServiceGrpc;

	public static void main(String[] args) {
		SpringApplication.run(Lab1Application.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void startServer() throws IOException, InterruptedException {
		medicationServiceGrpc.startServer();

	}
}