package com.lab1.lab1.entities;

import javax.persistence.*;

@Entity
@Table(name="doctor")
public class Doctor {
    @Id
    @GeneratedValue
    private long id;
    @Column(name="nume")
    private String nume;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", nume='" + nume + '\'' +
                '}';
    }
}
