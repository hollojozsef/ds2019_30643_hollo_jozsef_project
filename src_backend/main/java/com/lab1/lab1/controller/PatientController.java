package com.lab1.lab1.controller;

import com.lab1.lab1.entities.Patient;
import com.lab1.lab1.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/patient")
@CrossOrigin(origins = "http://localhost:4200")
public class PatientController {
    @Autowired
    PatientService service;

    @GetMapping("/all")
    public List<Patient> getAllPatient() {
        List<Patient> list = service.getAllPatients();
        return list;
    }

    @GetMapping("/find/{id}")
    public Patient getPatient(@PathVariable long id) {
        return service.getPatientById(id);
    }

    @DeleteMapping("/delete/{id}")
    public boolean deletePatient(@PathVariable long id) {
        service.deletePatient(id);
        return true;
    }

    @PutMapping("/update")
    public Patient updatePatient(@RequestBody Patient patient) {
        return service.setPatient(patient);
    }

    @PostMapping("/add")
    public Patient createCaregiver(@RequestBody Patient patient) {
        return service.setPatient(patient);
    }
}
