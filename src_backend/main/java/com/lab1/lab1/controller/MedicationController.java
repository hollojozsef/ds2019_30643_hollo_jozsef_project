package com.lab1.lab1.controller;

import com.lab1.lab1.entities.Caregiver;
import com.lab1.lab1.entities.Medication;
import com.lab1.lab1.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medication")
@CrossOrigin(origins = "http://localhost:4200")
public class MedicationController {
    @Autowired
    MedicationService service;

    @GetMapping("/all")
    public List<Medication> getAllMedication() {
        List<Medication> list = service.getAllMedication();
        return list;
    }

    @GetMapping("/find/{id}")
    public Medication getMedication(@PathVariable long id) {
        return service.getMedicationById(id);
    }

    @DeleteMapping("/delete/{id}")
    public boolean deleteMedication(@PathVariable long id) {
        service.deleteMedication(id);
        return true;
    }

    @PutMapping("/update")
    public Medication updateMedication(@RequestBody Medication medication) {
        return service.setMedication(medication);
    }

    @PostMapping("/add")
    public Medication createMedication(@RequestBody Medication medication) {
        return service.setMedication(medication);
    }
}
