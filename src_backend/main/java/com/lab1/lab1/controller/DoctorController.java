package com.lab1.lab1.controller;

import com.lab1.lab1.service.DoctorService;
import com.lab1.lab1.entities.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/doctor")
@CrossOrigin(origins = "http://localhost:4200")
public class DoctorController {
    @Autowired
    DoctorService service;

    @GetMapping("/all")
    public List<Doctor> getAllDoctors(){
        List<Doctor> list = service.getAllDoctors();
        return  list;
    }
    @GetMapping("/find/{id}")
    public Doctor getDoctor(@PathVariable long id){
         return service.getDoctorById(id);
    }
    @DeleteMapping("/delete/{id}")
    public boolean deleteDoctor(@PathVariable long id){
        service.deleteDoctor(id);
        return true;
    }
    @PutMapping("/update")
    public Doctor updateDoctor(Doctor doctor){
        return service.setDoctor(doctor);
    }
    @PostMapping("/add")
    public Doctor createDoctor(Doctor doctor){
        return service.setDoctor(doctor);
    }
}
