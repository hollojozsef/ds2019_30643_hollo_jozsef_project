package com.lab1.lab1.service;

import com.lab1.lab1.entities.Caregiver;
import com.lab1.lab1.entities.Patient;
import com.lab1.lab1.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PatientService {
    @Autowired
    PatientRepository repository;
    public List<Patient> getAllPatients(){
        List<Patient> caregiverList=repository.findAll();
        if(caregiverList.size()>0)
            return caregiverList;
        else
            return new ArrayList<Patient>();
    }
    public Patient getPatientById(long id){
        return repository.findById(id);
    }
    public void addPatient(Patient patient){
        repository.save(patient);
    }
    public void deletePatient(long id){
        repository.deleteById(id);
    }
    public Patient setPatient(Patient patient){
        return repository.save(patient);
    }
}
