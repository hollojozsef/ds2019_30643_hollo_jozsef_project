package com.lab1.lab1.service;

import com.lab1.lab1.entities.Caregiver;
import com.lab1.lab1.entities.Doctor;
import com.lab1.lab1.repository.CaregiverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CaregiverService {
    @Autowired
    CaregiverRepository repository;
    public List<Caregiver> getAllCaregivers(){
        List<Caregiver> caregiverList=repository.findAll();
        if(caregiverList.size()>0)
            return caregiverList;
        else
            return new ArrayList<Caregiver>();
    }
    public Caregiver getCaregiverById(long id){
        return repository.findById(id);
    }
    public void addCaregiver(Caregiver caregiver){
        repository.save(caregiver);
    }
    public void deleteCaregiver(long id){
        repository.deleteById(id);
    }
    public Caregiver setCaregiver(Caregiver caregiver){
        return repository.save(caregiver);
    }
}
