package com.lab1.lab1.service;

import com.lab1.lab1.entities.Medication;
import com.lab1.lab1.repository.MedicationRepository;
import com.yrrhelp.grpc.MedicationOuterClass;
import com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan;
import com.yrrhelp.grpc.medicationGrpc;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicationServiceGrpc extends medicationGrpc.medicationImplBase {
    @Autowired
    MedicationRepository medicationRepository;

    @Override
    public void getMedicationPlan(MedicationOuterClass.SendId request, StreamObserver<MedicationPlan> responseObserver) {

        Collection<Medication> medications=medicationRepository.findAll();
        List<MedicationPlan.Medication> medicationsForPlan=medications.stream().map(med->MedicationPlan.Medication.newBuilder()
        .setId((int)med.getId())
        .setDosage(med.getDosage())
        .setName(med.getName())
        .setSideEffect(med.getSideEffects()).build()).collect(Collectors.toList());
        MedicationPlan medicationPlan = MedicationPlan.newBuilder().addAllMedications(medicationsForPlan).build();
        responseObserver.onNext(medicationPlan);
        responseObserver.onCompleted();
    }

    @Override
    public void getResponseFromPatient(MedicationOuterClass.SendResponse request, StreamObserver<MedicationOuterClass.Empty> responseObserver) {
        int idPatent=request.getIdPatient();
        int idMedication = request.getIdMedication();
        if(request.getTaken()){
            System.out.println("Pacientul cu "+idPatent+" si-a luat medicamentul cu id" + idMedication+"!");
        }else{
            System.out.println("Pacientul cu "+idPatent+" NU si-a luat medicamentul!" + idMedication+"!");
        }
        responseObserver.onNext(MedicationOuterClass.Empty.newBuilder().build());
        responseObserver.onCompleted();
    }
    public void startServer() throws IOException, InterruptedException {
        Server server = ServerBuilder.forPort(9090).addService(this).build();
        server.start();
        System.out.println("Server started at futu-va an gura" + server.getPort());
        server.awaitTermination();
    }
}
