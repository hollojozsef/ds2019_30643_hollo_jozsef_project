package com.yrrhelp.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: medication.proto")
public final class medicationGrpc {

  private medicationGrpc() {}

  public static final String SERVICE_NAME = "medication";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.yrrhelp.grpc.MedicationOuterClass.LoginRequest,
      com.yrrhelp.grpc.MedicationOuterClass.APIResponse> getLoginMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "login",
      requestType = com.yrrhelp.grpc.MedicationOuterClass.LoginRequest.class,
      responseType = com.yrrhelp.grpc.MedicationOuterClass.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.yrrhelp.grpc.MedicationOuterClass.LoginRequest,
      com.yrrhelp.grpc.MedicationOuterClass.APIResponse> getLoginMethod() {
    io.grpc.MethodDescriptor<com.yrrhelp.grpc.MedicationOuterClass.LoginRequest, com.yrrhelp.grpc.MedicationOuterClass.APIResponse> getLoginMethod;
    if ((getLoginMethod = medicationGrpc.getLoginMethod) == null) {
      synchronized (medicationGrpc.class) {
        if ((getLoginMethod = medicationGrpc.getLoginMethod) == null) {
          medicationGrpc.getLoginMethod = getLoginMethod = 
              io.grpc.MethodDescriptor.<com.yrrhelp.grpc.MedicationOuterClass.LoginRequest, com.yrrhelp.grpc.MedicationOuterClass.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medication", "login"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yrrhelp.grpc.MedicationOuterClass.LoginRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yrrhelp.grpc.MedicationOuterClass.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new medicationMethodDescriptorSupplier("login"))
                  .build();
          }
        }
     }
     return getLoginMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.yrrhelp.grpc.MedicationOuterClass.Empty,
      com.yrrhelp.grpc.MedicationOuterClass.APIResponse> getLogoutMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "logout",
      requestType = com.yrrhelp.grpc.MedicationOuterClass.Empty.class,
      responseType = com.yrrhelp.grpc.MedicationOuterClass.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.yrrhelp.grpc.MedicationOuterClass.Empty,
      com.yrrhelp.grpc.MedicationOuterClass.APIResponse> getLogoutMethod() {
    io.grpc.MethodDescriptor<com.yrrhelp.grpc.MedicationOuterClass.Empty, com.yrrhelp.grpc.MedicationOuterClass.APIResponse> getLogoutMethod;
    if ((getLogoutMethod = medicationGrpc.getLogoutMethod) == null) {
      synchronized (medicationGrpc.class) {
        if ((getLogoutMethod = medicationGrpc.getLogoutMethod) == null) {
          medicationGrpc.getLogoutMethod = getLogoutMethod = 
              io.grpc.MethodDescriptor.<com.yrrhelp.grpc.MedicationOuterClass.Empty, com.yrrhelp.grpc.MedicationOuterClass.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medication", "logout"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yrrhelp.grpc.MedicationOuterClass.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yrrhelp.grpc.MedicationOuterClass.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new medicationMethodDescriptorSupplier("logout"))
                  .build();
          }
        }
     }
     return getLogoutMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.yrrhelp.grpc.MedicationOuterClass.SendId,
      com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan> getGetMedicationPlanMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getMedicationPlan",
      requestType = com.yrrhelp.grpc.MedicationOuterClass.SendId.class,
      responseType = com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.yrrhelp.grpc.MedicationOuterClass.SendId,
      com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan> getGetMedicationPlanMethod() {
    io.grpc.MethodDescriptor<com.yrrhelp.grpc.MedicationOuterClass.SendId, com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan> getGetMedicationPlanMethod;
    if ((getGetMedicationPlanMethod = medicationGrpc.getGetMedicationPlanMethod) == null) {
      synchronized (medicationGrpc.class) {
        if ((getGetMedicationPlanMethod = medicationGrpc.getGetMedicationPlanMethod) == null) {
          medicationGrpc.getGetMedicationPlanMethod = getGetMedicationPlanMethod = 
              io.grpc.MethodDescriptor.<com.yrrhelp.grpc.MedicationOuterClass.SendId, com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medication", "getMedicationPlan"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yrrhelp.grpc.MedicationOuterClass.SendId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan.getDefaultInstance()))
                  .setSchemaDescriptor(new medicationMethodDescriptorSupplier("getMedicationPlan"))
                  .build();
          }
        }
     }
     return getGetMedicationPlanMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.yrrhelp.grpc.MedicationOuterClass.SendResponse,
      com.yrrhelp.grpc.MedicationOuterClass.Empty> getGetResponseFromPatientMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getResponseFromPatient",
      requestType = com.yrrhelp.grpc.MedicationOuterClass.SendResponse.class,
      responseType = com.yrrhelp.grpc.MedicationOuterClass.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.yrrhelp.grpc.MedicationOuterClass.SendResponse,
      com.yrrhelp.grpc.MedicationOuterClass.Empty> getGetResponseFromPatientMethod() {
    io.grpc.MethodDescriptor<com.yrrhelp.grpc.MedicationOuterClass.SendResponse, com.yrrhelp.grpc.MedicationOuterClass.Empty> getGetResponseFromPatientMethod;
    if ((getGetResponseFromPatientMethod = medicationGrpc.getGetResponseFromPatientMethod) == null) {
      synchronized (medicationGrpc.class) {
        if ((getGetResponseFromPatientMethod = medicationGrpc.getGetResponseFromPatientMethod) == null) {
          medicationGrpc.getGetResponseFromPatientMethod = getGetResponseFromPatientMethod = 
              io.grpc.MethodDescriptor.<com.yrrhelp.grpc.MedicationOuterClass.SendResponse, com.yrrhelp.grpc.MedicationOuterClass.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medication", "getResponseFromPatient"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yrrhelp.grpc.MedicationOuterClass.SendResponse.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yrrhelp.grpc.MedicationOuterClass.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new medicationMethodDescriptorSupplier("getResponseFromPatient"))
                  .build();
          }
        }
     }
     return getGetResponseFromPatientMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static medicationStub newStub(io.grpc.Channel channel) {
    return new medicationStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static medicationBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new medicationBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static medicationFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new medicationFutureStub(channel);
  }

  /**
   */
  public static abstract class medicationImplBase implements io.grpc.BindableService {

    /**
     */
    public void login(com.yrrhelp.grpc.MedicationOuterClass.LoginRequest request,
        io.grpc.stub.StreamObserver<com.yrrhelp.grpc.MedicationOuterClass.APIResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getLoginMethod(), responseObserver);
    }

    /**
     */
    public void logout(com.yrrhelp.grpc.MedicationOuterClass.Empty request,
        io.grpc.stub.StreamObserver<com.yrrhelp.grpc.MedicationOuterClass.APIResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getLogoutMethod(), responseObserver);
    }

    /**
     */
    public void getMedicationPlan(com.yrrhelp.grpc.MedicationOuterClass.SendId request,
        io.grpc.stub.StreamObserver<com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan> responseObserver) {
      asyncUnimplementedUnaryCall(getGetMedicationPlanMethod(), responseObserver);
    }

    /**
     */
    public void getResponseFromPatient(com.yrrhelp.grpc.MedicationOuterClass.SendResponse request,
        io.grpc.stub.StreamObserver<com.yrrhelp.grpc.MedicationOuterClass.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getGetResponseFromPatientMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getLoginMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.yrrhelp.grpc.MedicationOuterClass.LoginRequest,
                com.yrrhelp.grpc.MedicationOuterClass.APIResponse>(
                  this, METHODID_LOGIN)))
          .addMethod(
            getLogoutMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.yrrhelp.grpc.MedicationOuterClass.Empty,
                com.yrrhelp.grpc.MedicationOuterClass.APIResponse>(
                  this, METHODID_LOGOUT)))
          .addMethod(
            getGetMedicationPlanMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.yrrhelp.grpc.MedicationOuterClass.SendId,
                com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan>(
                  this, METHODID_GET_MEDICATION_PLAN)))
          .addMethod(
            getGetResponseFromPatientMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.yrrhelp.grpc.MedicationOuterClass.SendResponse,
                com.yrrhelp.grpc.MedicationOuterClass.Empty>(
                  this, METHODID_GET_RESPONSE_FROM_PATIENT)))
          .build();
    }
  }

  /**
   */
  public static final class medicationStub extends io.grpc.stub.AbstractStub<medicationStub> {
    private medicationStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationStub(channel, callOptions);
    }

    /**
     */
    public void login(com.yrrhelp.grpc.MedicationOuterClass.LoginRequest request,
        io.grpc.stub.StreamObserver<com.yrrhelp.grpc.MedicationOuterClass.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getLoginMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void logout(com.yrrhelp.grpc.MedicationOuterClass.Empty request,
        io.grpc.stub.StreamObserver<com.yrrhelp.grpc.MedicationOuterClass.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getLogoutMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getMedicationPlan(com.yrrhelp.grpc.MedicationOuterClass.SendId request,
        io.grpc.stub.StreamObserver<com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetMedicationPlanMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getResponseFromPatient(com.yrrhelp.grpc.MedicationOuterClass.SendResponse request,
        io.grpc.stub.StreamObserver<com.yrrhelp.grpc.MedicationOuterClass.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetResponseFromPatientMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class medicationBlockingStub extends io.grpc.stub.AbstractStub<medicationBlockingStub> {
    private medicationBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.yrrhelp.grpc.MedicationOuterClass.APIResponse login(com.yrrhelp.grpc.MedicationOuterClass.LoginRequest request) {
      return blockingUnaryCall(
          getChannel(), getLoginMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.yrrhelp.grpc.MedicationOuterClass.APIResponse logout(com.yrrhelp.grpc.MedicationOuterClass.Empty request) {
      return blockingUnaryCall(
          getChannel(), getLogoutMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan getMedicationPlan(com.yrrhelp.grpc.MedicationOuterClass.SendId request) {
      return blockingUnaryCall(
          getChannel(), getGetMedicationPlanMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.yrrhelp.grpc.MedicationOuterClass.Empty getResponseFromPatient(com.yrrhelp.grpc.MedicationOuterClass.SendResponse request) {
      return blockingUnaryCall(
          getChannel(), getGetResponseFromPatientMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class medicationFutureStub extends io.grpc.stub.AbstractStub<medicationFutureStub> {
    private medicationFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.yrrhelp.grpc.MedicationOuterClass.APIResponse> login(
        com.yrrhelp.grpc.MedicationOuterClass.LoginRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getLoginMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.yrrhelp.grpc.MedicationOuterClass.APIResponse> logout(
        com.yrrhelp.grpc.MedicationOuterClass.Empty request) {
      return futureUnaryCall(
          getChannel().newCall(getLogoutMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan> getMedicationPlan(
        com.yrrhelp.grpc.MedicationOuterClass.SendId request) {
      return futureUnaryCall(
          getChannel().newCall(getGetMedicationPlanMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.yrrhelp.grpc.MedicationOuterClass.Empty> getResponseFromPatient(
        com.yrrhelp.grpc.MedicationOuterClass.SendResponse request) {
      return futureUnaryCall(
          getChannel().newCall(getGetResponseFromPatientMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_LOGIN = 0;
  private static final int METHODID_LOGOUT = 1;
  private static final int METHODID_GET_MEDICATION_PLAN = 2;
  private static final int METHODID_GET_RESPONSE_FROM_PATIENT = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final medicationImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(medicationImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_LOGIN:
          serviceImpl.login((com.yrrhelp.grpc.MedicationOuterClass.LoginRequest) request,
              (io.grpc.stub.StreamObserver<com.yrrhelp.grpc.MedicationOuterClass.APIResponse>) responseObserver);
          break;
        case METHODID_LOGOUT:
          serviceImpl.logout((com.yrrhelp.grpc.MedicationOuterClass.Empty) request,
              (io.grpc.stub.StreamObserver<com.yrrhelp.grpc.MedicationOuterClass.APIResponse>) responseObserver);
          break;
        case METHODID_GET_MEDICATION_PLAN:
          serviceImpl.getMedicationPlan((com.yrrhelp.grpc.MedicationOuterClass.SendId) request,
              (io.grpc.stub.StreamObserver<com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan>) responseObserver);
          break;
        case METHODID_GET_RESPONSE_FROM_PATIENT:
          serviceImpl.getResponseFromPatient((com.yrrhelp.grpc.MedicationOuterClass.SendResponse) request,
              (io.grpc.stub.StreamObserver<com.yrrhelp.grpc.MedicationOuterClass.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class medicationBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    medicationBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.yrrhelp.grpc.MedicationOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("medication");
    }
  }

  private static final class medicationFileDescriptorSupplier
      extends medicationBaseDescriptorSupplier {
    medicationFileDescriptorSupplier() {}
  }

  private static final class medicationMethodDescriptorSupplier
      extends medicationBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    medicationMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (medicationGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new medicationFileDescriptorSupplier())
              .addMethod(getLoginMethod())
              .addMethod(getLogoutMethod())
              .addMethod(getGetMedicationPlanMethod())
              .addMethod(getGetResponseFromPatientMethod())
              .build();
        }
      }
    }
    return result;
  }
}
