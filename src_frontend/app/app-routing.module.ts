import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DoctorComponent} from './doctor/doctor.component';
import {DoctorListComponent} from './doctor-list/doctor-list.component';
import { CaregiverListComponent } from './caregiver-list/caregiver-list.component';
import { CaregiverFormComponent } from './caregiver-form/caregiver-form.component';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientFormComponent } from './patient-form/patient-form.component';
import { MedicationListComponent } from './medication-list/medication-list.component';
import { MedicationFormComponent } from './medication-form/medication-form.component';
import { LoginComponent } from './login/login.component';
import {ChartComponent} from './chart/chart.component';
import {PatientChartComponent} from './patient-chart/patient-chart.component';
import {PatientHistoryComponent} from './patient-history/patient-history.component';
import {SuggestionComponent} from './suggestion/suggestion.component';

const routes: Routes = [{path: 'doctor' , component : DoctorComponent},
{path: 'all' , component : DoctorListComponent},
{path: 'caregiver', component: CaregiverListComponent},
{path: 'caregivercreate', component: CaregiverFormComponent},
{path: 'patient', component: PatientListComponent},
{path: 'patientcreate', component: PatientFormComponent},
{path: 'medication', component: MedicationListComponent},
{path: 'medicationcreate', component: MedicationFormComponent},
{path: 'login', component: LoginComponent},
  {path: '', component: LoginComponent},
  {path: 'logout', component: LoginComponent},
  {path: 'hello-world', component: DoctorComponent},
  {path: 'dailyChart', component: PatientChartComponent},
  {path: 'patientList' , component: PatientHistoryComponent},
  {path: 'suggestion' , component : SuggestionComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents=[DoctorComponent];
