import { Component, OnInit } from '@angular/core';
import { Medication } from '../medication';
import { MedicationService } from '../service/medication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-medication-list',
  templateUrl: './medication-list.component.html',
  styleUrls: ['./medication-list.component.css']
})
export class MedicationListComponent implements OnInit {
  medications:Medication[];
  constructor(private medicationService:MedicationService,private _router:Router) { }

  ngOnInit() {
    this.medicationService.findAll().subscribe(data=>{
      this.medications=data;});
  }
updateMedication(medication){
  this.medicationService.setter(medication);
  this._router.navigate(['medicationcreate']);
}
newMedication(){
  let medication=new Medication();
  this.medicationService.setter(medication);
  this._router.navigate(['medicationcreate']);
}
deleteMedication(medication){
  this.medicationService.deleteMedication(medication.id).subscribe((data)=>{
    this.medications.splice(this.medications.indexOf(medication),1);
  })
}
  goBack(){
  this._router.navigate(['caregiver']);
  }
}
