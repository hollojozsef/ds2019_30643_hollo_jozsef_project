import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Doctor} from '../doctor';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  private baseUrl:string;
  constructor(private http: HttpClient) {
    this.baseUrl="http://localhost:8080/doctor/all";
   }
   public findAll():Observable<Doctor[]>{
     return this.http.get<Doctor[]>(this.baseUrl);
   }
}
