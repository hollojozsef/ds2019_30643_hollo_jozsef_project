import { Caregiver } from './caregiver';

export class Patient {
    public id:number;
    private name:string;
    private gender:string ;
    private address:string ;
    private medicalRecord:string ;
}
