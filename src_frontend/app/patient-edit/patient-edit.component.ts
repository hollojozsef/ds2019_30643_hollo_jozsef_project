import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PatientService } from '../service/patient.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
@Component({
  selector: 'app-patient-edit',
  templateUrl: './patient-edit.component.html',
  styleUrls: ['./patient-edit.component.css']
})
export class PatientEditComponent implements OnInit {

  constructor(private router: Router, private patient: PatientService, private formBuilder: FormBuilder) { }
  patientForm: FormGroup;
  _id:string='';
  name:string='';
  gender:string='';
  address:string='';
  medicalRecord:string='';
  isLoadingResults = false;
  ngOnInit() {
    this.patientForm = this.formBuilder.group({
      'name' : [null, Validators.required],
      'gender' : [null, Validators.required],
      'address' : [null, Validators.required],
      'medicalRecord' : [null, Validators.required]
    });
  }


  onFormSubmit(form:NgForm) {
    this.isLoadingResults = true;
  this.patient.updatePatient(this._id)
    .subscribe(res => {
        let id = res['_id'];
        this.isLoadingResults = false;
        this.router.navigate(['/patientcreate', id]);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      }
    );
  }
  patientDetails() {
    this.router.navigate(['/patientcreate', this._id]);
  }
}
