import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { ChartsModule } from 'angular-bootstrap-md';

import { AppRoutingModule,routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { DoctorComponent } from './doctor/doctor.component';
import { DoctorService } from './service/doctor.service';
import { CaregiverService } from './service/caregiver.service';
import { PatientService } from './service/patient.service';
import { MedicationService } from './service/medication.service';
import {DoctorListComponent} from './doctor-list/doctor-list.component';
import { CaregiverListComponent } from './caregiver-list/caregiver-list.component';
import { CaregiverFormComponent } from './caregiver-form/caregiver-form.component';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientFormComponent } from './patient-form/patient-form.component';
import { MedicationListComponent } from './medication-list/medication-list.component';
import { MedicationFormComponent } from './medication-form/medication-form.component';
import { PatientEditComponent } from './patient-edit/patient-edit.component';
import { LoginComponent } from './login/login.component';
import { HttpInterceptorService } from './service/http-interceptor.service';
import { MenuComponent } from './menu/menu.component';
import { ChartComponent } from './chart/chart.component';
import {ChartModule} from '@syncfusion/ej2-angular-charts';
import { PatientChartComponent } from './patient-chart/patient-chart.component';
import { SuggestionComponent } from './suggestion/suggestion.component';
import { PatientHistoryComponent } from './patient-history/patient-history.component';
import {LogoutComponent} from './logout/logout.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    DoctorComponent,
    DoctorListComponent,
    CaregiverListComponent,
    CaregiverFormComponent,
    PatientListComponent,
    PatientFormComponent,
    MedicationListComponent,
    MedicationFormComponent,
    PatientEditComponent,
    LoginComponent,
    MenuComponent,
    ChartComponent,
    PatientChartComponent,
    SuggestionComponent,
    PatientHistoryComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ChartsModule,
    ChartModule
  ],
  providers: [DoctorService,
    CaregiverService,
    PatientService,
    MedicationService,

  {
    provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
